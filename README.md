# Numerical Schrodinger

params.txt:

  Simulation parameters file.
  ...


schrodinger.c:

 FFTW3 must be already installed
 Having installed it the standard way, the line below should compile the code.

 gcc -std=c99 test.c -I/usr/include -I/usr/local/lib/ -o test -lfftw3 -lm -static


output.dat:

 Binary output of test.c written as follows:
 (fftw_complex) x[i] (fftw_complex) phi[i] (fftw_complex) fphi[i]

viewer.cxx:

  It's a ROOT macro, which plots result of Fourier transform, as well as the state before transformation.
