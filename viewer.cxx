typedef double fftw_complex[2];   // same type is used in fftw3.h

TGraph* EnergyGraph(string name, double dt, double tf, Color_t color);

void viewer() {

  int N ;
  double hbar, m, dx, a, w, dt, tf;

  FILE *input_params = fopen("params.txt", "r");
  fscanf(input_params, "%i%lf%lf%lf%lf%lf%lf%lf", &N, &dx, &a, &hbar, &m,  &w, &dt, &tf );
  fclose(input_params);

  fftw_complex x[N];
  fftw_complex psi[N];
  fftw_complex fpsi[N];

  FILE* data = fopen("output.dat", "rb");
  for(int i=0; i<N; ++i){
    fread(&x[i], sizeof(fftw_complex), 1, data);
    fread(&psi[i], sizeof(fftw_complex), 1, data);
    fread(&fpsi[i], sizeof(fftw_complex), 1, data);
  }

  TF1* hermite = new TF1("hermite", "[2]*TMath::Power( [0]/TMath::Pi()/[1], 1./4.)*TMath::Exp(-1*[0]*x*x/2/[1]) ", x[0][0], x[N-1][0]);

  hermite->SetNpx(10000);
  hermite->SetParameter(0, m*w);
  hermite->SetParameter(1, hbar);
  hermite->SetParameter(2, 1.);

  hermite->SetLineColor(kGreen-3);
  TGraph **g = new TGraph*[2];
  double xg[N];
  double psig[N];
  double fpsig[N];
  for(int i=0; i<N; ++i){
    xg[i] = x[i][0];
    psig[i] = psi[i][0];
    fpsig[i] = fpsi[i][0];
  }


  g[0] = new TGraph(N, xg, psig);
  g[1] = new TGraph(N, xg, fpsig);
  g[0]->SetTitle(Form("N=%i, dx=%.3lf, a=%.2lf, #hbar=%.2lf, m=%.2lf, #omega=%.2lf, dt=%.3lf, t_{f}=%.2lf ", N, dx, a, hbar, m,  w, dt, tf));
  g[0]->SetMarkerStyle(8);
  g[0]->SetMarkerColor(kRed);
  g[0]->SetLineColor(kRed);

  g[1]->SetMarkerStyle(8);
  g[1]->SetMarkerColor(kBlue);
  g[1]->SetLineColor(kBlue);

  g[0]->GetYaxis()->SetRangeUser( 0, 1.1);//g[0]->GetMaximum());
  g[0]->SetTitle("Shape comparison");
  g[0]->Draw("APC");
  g[1]->Draw("PC");
  hermite->Draw("same");

  auto l = new TLegend(0.7, 0.6, 0.9, 0.9);
  l->AddEntry(g[1], "#psi_{initial}");
  l->AddEntry(g[0], "Numerical #psi_{0} ");
  l->AddEntry(hermite, "Analytical #psi_{0} ");
  l->Draw();

  TCanvas* ecanv = new TCanvas();
  auto e1 = EnergyGraph("energy.dat", dt, tf, kBlue);
  auto e2 = EnergyGraph("energy1.dat", dt, tf, kBlack);
  e1->Draw("ALP");
  e2->Draw("LP");
  TF1* e0 = new TF1("e0", "[0]", 0, static_cast<int>(tf/dt)+1);
  e0->SetParameter(0, hbar*w/2.);
  e0->SetLineColor(kRed);
  e0->Draw("same");

}

TGraph* EnergyGraph(string name, double dt, double tf, Color_t color){
  int EN = static_cast<int>(tf/dt)+1;
  double time[EN];
  double energy[EN];
  FILE* energy_data = fopen(name.c_str(), "rb");
  for(int i=0; i<EN; ++i){
    fread(&time[i], sizeof(double), 1, energy_data);
    fread(&energy[i], sizeof(double), 1, energy_data);
  }

  auto e_graph = new TGraph(EN, time, energy);
  e_graph->SetTitle("Energy at given iteration");
  e_graph->SetLineColor(color);
  return e_graph;
}
