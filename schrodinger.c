// FFTW3 must be already installed
// Having installed it the standard way, the line below should compile the code.
// gcc -std=c99 schrodinger.c -I/usr/include -I/usr/local/lib/ -o schrodinger -lfftw3 -lm -static
#include <complex.h> // for operations on fftw_complex
#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

const double PI = 3.14159265;

double momentum(int _m, int _N, double _dx);
fftw_complex* d2dx2(int _N, fftw_complex* _in, double _dx );

int main(){

  int N ;
  double hbar, m, dx, a, w, dt, tf;

  FILE *input_params = fopen("params.txt", "r");
  fscanf(input_params, "%i%lf%lf%lf%lf%lf%lf%lf", &N, &dx, &a, &hbar, &m,  &w, &dt, &tf );
  fclose(input_params);

  fftw_complex x[N], psi[N];   // x, wave function values
  // to save the initial state (for comparison purposes)
  fftw_complex initpsi[N];

  // INITIAL STATE
  for(int i=0; i<N; ++i){
    x[i] =  (i - N/2.)*dx;
    //psi[i] = pow( m*w/PI/hbar, 0.25) * exp(-1*m*w*x[i]*x[i]/2/hbar);
    psi[i] = sqrt(a/PI) * exp(-a * x[i] * x[i]  );
    initpsi[i] = psi[i];
  }

 FILE *energy_file = fopen("energy.dat", "wb"); //binary output file
 double E=0.;
  // PERFORM TIME EVOLUTION:
 for (double t = 0; t < tf; t+=dt) {
    E = 0;
    fftw_complex* secDeriv = d2dx2(N, psi, dx); // second derivative
    double norm = 0.;
    for(int i=0; i<N; ++i){
        // CALCULATE H*Psi
        secDeriv[i] *= -1*hbar*hbar/(2*m) ;
        secDeriv[i] +=  m*w*w/2 * x[i] * x[i] * psi[i] ; // here H|psi> is completed
        // CALCULATE Psi(x, t+dt)
        psi[i] -= secDeriv[i]*dt/hbar; // calculate |psi(x, t+dt)>
        // CALCULATE NORMALIZATION FACTOR \integral psi^*psi dx is approximated by Sum of psi^*[i]psi[i]
        fftw_complex psibar = conj(psi[i]); // calculate psi^*

        norm += psibar*psi[i]*dx;
        // CALCULATE dENERGY
        E += psibar*secDeriv[i]*dx;
      //  norm += psi[i];
    }
    // NORMALIZE
    norm = sqrt( norm );
    for(int i=0; i<N; ++i) psi[i] /= norm;

    // write energy at each iteration to a file
    fwrite( &t, sizeof(double), 1, energy_file);
    fwrite( &E, sizeof(double), 1, energy_file);
    printf("Energy: %.3lf at time %.3lf\r", E, t );
  }
  // PRINT FINAL ENERGY
  fclose(energy_file);
  printf("Energy: %.3lf at time %.3lf\n", E, tf );

  // Save the binary output to output.dat

  FILE *output = fopen("output.dat", "wb"); //binary output file
  for(int i=0; i<N; ++i){
    fwrite( &x[i], sizeof(fftw_complex), 1, output);
    fwrite( &psi[i], sizeof(fftw_complex), 1, output);
    fwrite( &initpsi[i], sizeof(fftw_complex), 1, output);
  }

  fclose(output);
  return 0;
}










double momentum(int _m, int _N, double _dx){
  if( _m > _N/2 )    return 2.*PI*(_m - _N)/( _N * _dx);
  else               return 2.*PI*_m/( _N * _dx);

  return 0; // never used
}

fftw_complex* d2dx2(int _N, fftw_complex* _in, double _dx ){

  fftw_complex* output = (fftw_complex*) malloc (_N*sizeof(fftw_complex));
  fftw_plan plan;
  // Fourier transform
  plan = fftw_plan_dft_1d(_N, _in, output, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  for(int i=0; i<_N; ++i)  output[i] *= -1* momentum(i, _N, _dx)*momentum(i, _N, _dx);

  // inverse Fourier transform
  plan = fftw_plan_dft_1d(_N, output, output, FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan);

  for(int i=0; i<_N; ++i)  output[i] /= _N;

  fftw_destroy_plan(plan);
  fftw_cleanup();
  return output;

}
